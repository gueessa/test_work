<?php

/**
 * Get an item from an array using "dot" notation.
 *
 * @param   array   $array
 * @param   string  $key
 * @param   mixed   $default
 * @return  mixed
 */
function array_get($array, $key, $default = null)
{
    if (is_null($key)) return $array;

    foreach (explode('.', $key) as $segment)
    {
        if ( ! is_array($array) or ! array_key_exists($segment, $array))
        {
            return value($default);
        }
    
        $array = $array[$segment];
    }
    
    return $array;
}

/**
 * Return the value of the given item.
 *
 * If the given item is a Closure the result of the Closure will be returned.
 *
 * @param   mixed   $value
 * @return  mixed
 */
function value($value)
{
    return (is_callable($value) and ! is_string($value)) ? call_user_func($value) : $value;
}