<?php

//
define('DS', DIRECTORY_SEPARATOR);
define('EXT', '.php');
define('BASEPATH', __DIR__.DS);

//
require_once BASEPATH.'helper'.EXT; 
require_once BASEPATH.'libraries'.DS.'autoloader'.EXT;

class Core {
    
    /**
     * Run
     * 
     * https://launchpad.37signals.com/
     * login: gueessa
     * password: 1q2w3e4r
     * 
     * @return  void
     */
    public static function run()
    {
        Event::fire('logger', array('webhook run'));
        
        $helpscout = Api::make('helpscout');
        $helpscout->setToken(Config::get('api.helpscout.token'));
        
        $webhook = $helpscout->getWebhook();
        
        if ($webhook->isValid()) 
        {
            $eventType = $webhook->getEventType();
            
            switch($eventType) 
            {
                case 'convo.created':
                    $conv = $webhook->getConversation();
                    $task = $conv->getSubject();
                    
                    Event::fire('logger', array($task));
                    
                    Event::fire('convo.created', array($task));
                    break;
            }
        }
    }
}

Event::listen('convo.created', function($task) {
    $highrise = Api::make('highrise');
    $highrise->debug = Config::get('api.highrise.debug');
    $highrise->setAccount(Config::get('api.highrise.account'));
    $highrise->setToken(Config::get('api.highrise.token'));
    
    $task = new Highrise_Task($highrise);
    $task->setBody($task);
    $task->setFrame("today");
    $task->save();
});

Event::listen('logger', function($data)
{
     Log::write('info', $data, true);
});


// Run application
Core::run();