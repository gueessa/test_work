<?php

class Log {

    /**
     * Write a message to the log file.
     *
     * @param string $type
     * @param string $message
     * @return void
     */
    public static function write($type, $message, $pretty_print = false)
    {
        $message = ($pretty_print) ? print_r($message, true) : $message;
    
        $message = static::format($type, $message);

        file_put_contents(BASEPATH.'logs/'.date('Y-m-d').'.log', $message, LOCK_EX | FILE_APPEND);
    }
    
    /**
     * Format a log message for logging.
     *
     * @param string $type
     * @param string $message
     * @return string
     */
    protected static function format($type, $message)
    {
        return date('Y-m-d H:i:s').' '.strtoupper($type)." - {$message}".PHP_EOL;
    }
    
    /**
     * Dynamically write a log message.
     *
     * @return  void
     */
    public static function __callStatic($method, $parameters)
    {
        $parameters[1] = (empty($parameters[1])) ? false : $parameters[1];
    
        static::write($method, $parameters[0], $parameters[1]);
    }
}