<?php

include __DIR__.DS.'HelpScout/Webhook.php';

class Api_Helpscout {

    /**
     * 
     * 
     */
    public $token;
    
    /**
     * Construnctor
     * 
     * @return  void
     */    
    public function __construct()
    {
        
    }
    
    /**
     * Set token
     * 
     * @param   string  $token
     * @return  void
     */
    public function setToken($token)
    {
        $this->token = $token;
    }
    
    /**
     * Get webhook
     * 
     * @return HelpScout/Webhook 
     */    
    public function getWebhook()
    {
        return new \HelpScout\Webhook($this->token);
    }
}