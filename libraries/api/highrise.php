<?php

class Api_Highrise {
    
    /**
     * 
     * 
     */
    public $account;
    
    /**
     * 
     * 
     */
    public $token;

    /**
     * 
     * 
     */
    protected $curl;

    /**
     * 
     * 
     */
    public $debug;  

	public function __construct()
    {
        $this->curl = curl_init();
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('Accept: application/xml', 'Content-Type: application/xml'));
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST,0);	
    }
    
	public function setAccount($account)
    {
        $this->account = $account;
    }

    public function setToken($token)
    {
        $this->token = $token;
        curl_setopt($this->curl, CURLOPT_USERPWD, $this->token.':x');
    }    
    
	protected function postDataWithVerb($path, $request_body, $verb = "POST")
    {
        $this->curl = curl_init();

        $url = "https://" . $this->account . ".highrisehq.com" . $path;

        if ($this->debug)
            print "postDataWithVerb $verb $url ============================\n";

        curl_setopt($this->curl, CURLOPT_URL,$url);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $request_body);
        
        if ($this->debug == true)
            curl_setopt($this->curl, CURLOPT_VERBOSE, true);

        curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('Accept: application/xml', 'Content-Type: application/xml'));
        curl_setopt($this->curl, CURLOPT_USERPWD,$this->token.':x');
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER,true);


        if ($verb != "POST")
            curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, $verb);
        else
            curl_setopt($this->curl, CURLOPT_POST, true);

        $ret = curl_exec($this->curl);

        if ($this->debug == true)
            print "Begin Request Body ============================\n" . $request_body . "End Request Body ==============================\n";

        curl_setopt($this->curl,CURLOPT_HTTPGET, true);

        return $ret;
    }

    protected function getURL($path)
    {
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('Accept: application/xml', 'Content-Type: application/xml'));
        curl_setopt($this->curl, CURLOPT_USERPWD,$this->token.':x');
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER,true);

        $url = "https://" . $this->account . ".highrisehq.com" . $path;

        if ($this->debug == true)
            curl_setopt($this->curl, CURLOPT_VERBOSE, true);


        curl_setopt($this->curl,CURLOPT_URL,$url);
        
        $response = curl_exec($this->curl);

        if ($this->debug == true)
            print "Response: =============\n" . $response . "============\n";

        return $response;
    }

    protected function getLastReturnStatus()
    {
        return curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
    }

    protected function getXMLObjectForUrl($url)
    {
        $xml = $this->getURL($url);
        $xml_object = simplexml_load_string($xml);
        
        return $xml_object;
    }

    protected function checkForErrors($type, $expected_status_codes = 200)
    {
        if (!is_array($expected_status_codes))
            $expected_status_codes = array($expected_status_codes);

        if (!in_array($this->getLastReturnStatus(), $expected_status_codes))
        {
            switch($this->getLastReturnStatus())
            {
                case 404:
                    throw new Exception("$type not found");
                    break;
                case 403:
                    throw new Exception("Access denied to $type resource");
                    break;
                case 507:
                    throw new Exception("Cannot create $type: Insufficient storage in your Highrise Account");
                    break;
                default:
                    throw new Exception("API for $type returned Status Code: " . $this->getLastReturnStatus() . " Expected Code: " . implode(",", $expected_status_codes));
                    break;
            }	
        }
    }        
} 


class Highrise_Task extends Api_Highrise
{
    private $highrise;
    public $id;
    public $author_id;
    public $subject_id;
    public $subject_type;
    public $subject_name;
    public $category_id;
    public $body;
    public $frame;
    public $due_at;
    public $alert_at;
    public $created_at;
    public $updated_at;
    public $public;
    public $recording_id;
    public $notify;
    public $owner_id;
    public $deleted;
    
    public function complete()
    {
        $task_xml = $this->toXML();
        $new_task_xml = $this->postDataWithVerb("/tasks/" . $this->getId() . "/complete.xml", "", "POST");
        $this->checkForErrors("Task", 200);	
        $this->loadFromXMLObject(simplexml_load_string($new_task_xml));

        return true;	
    }
    
    public function save()
    {
        if ($this->getFrame() == null)
            throw new Exception("You need to specify a valid time frame to save a task");
        
        if ($this->id == null) // Create
        {
            $task_xml = $this->toXML();
            $new_task_xml = $this->postDataWithVerb("/tasks.xml", $task_xml, "POST");
            $this->checkForErrors("Task", 201);	
            $this->loadFromXMLObject(simplexml_load_string($new_task_xml));
            return true;
        }
        else
        {
            $task_xml = $this->toXML();
            $new_task_xml = $this->postDataWithVerb("/tasks/" . $this->getId() . ".xml", $task_xml, "PUT");
            $this->checkForErrors("Task", 200);	
            return true;	
        }
    }
    
    public function delete()
    {
        $this->postDataWithVerb("/tasks/" . $this->getId() . ".xml", "", "DELETE");
        $this->checkForErrors("Task", 200);	
        $this->deleted = true;
    }
    
    public function assignToUser(HighriseUser $user)
    {
        $this->setOwnerId($user->getId());
    }
    
    public function setOwnerId($owner_id)
    {
        $this->owner_id = (string)$owner_id;
    }
    
    public function getOwnerId()
    {
        return $this->owner_id;
    }
    
    public function setNotify($notify)
    {
        if ($notify == "true" || $notify == true || $notify == 1)
            $notify = true;
        else
            $notify = false;
    
        $this->notify = (string) $notify;
    }
    
    public function getNotify()
    {
    return $this->notify;
    }
    
    public function setRecordingId($recording_id)
    {
        $this->recording_id = (string)$recording_id;
    }
    
    public function getRecordingId()
    {
        return $this->recording_id;
    }
    
    public function setPublic($public)
    {
        $this->public = (string)$public;
    }
    
    public function getPublic()
    {
        return $this->public;
    }
    
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = (string)$updated_at;
    }
    
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    public function setCreatedAt($created_at)
    {
        $this->created_at = (string)$created_at;
    }
    
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    public function setAlertAt($alert_at)
    {
        $this->alert_at = (string)$alert_at;
    }
    
    public function getAlertAt()
    {
        return $this->alert_at;
    }
    
    
    public function setDueAt($due_at)
    {
        $this->due_at = (string)$due_at;
    }
    
    public function getDueAt()
    {
        return $this->due_at;
    }
    
    public function setFrame($subject_type)
    {
        $valid_frames = array("today", "tomorrow", "this_week", "next_week", "later", "overdue");
        $frame = str_replace(" ", "_", strtolower($subject_type));
    
        if ($frame != null && !in_array($frame, $valid_frames))
            throw new Exception("$subject_type is not a valid frame. Available frames: " . implode(", ", $valid_frames));
    
        $this->frame = (string)$frame;
    }
    
    public function getFrame()
    {
        return $this->frame;
    }
    
    
    public function setBody($body)
    {
        $this->body = (string)$body;
    }
    
    public function getBody()
    {
        return $this->body;
    }
    
    public function setCategoryId($category_id)
    {
        $this->category_id = (string)$category_id;
    }
    
    public function getCategoryId()
    {
        return $this->category_id;
    }
    
    public function setSubjectName($subject_name)
    {
        $this->subject_name = (string)$subject_name;
    }
    
    public function getSubjectName()
    {
        return $this->subject_name;
    }
    
    public function setSubjectType($subject_type)
    {
        $valid_types = array("Party", "Company", "Deal", "Kase");
        $subject_type = ucwords(strtolower($subject_type));

        if ($subject_type != null && !in_array($subject_type, $valid_types))
            throw new Exception("$subject_type is not a valid subject type. Available subject types: " . implode(", ", $valid_types));
        
        $this->subject_type = (string)$subject_type;
    }
    
    public function getSubjectType()
    {
        return $this->subject_type;
    }
    
    public function setSubjectId($subject_id)
    {
        $this->subject_id = (string)$subject_id;
    }
    
    public function getSubjectId()
    {
        return $this->subject_id;
    }
    
    
    public function setAuthorId($author_id)
    {
        $this->author_id = (string)$author_id;
    }
    
    public function getAuthorId()
    {
        return $this->author_id;
    }
    
    
    public function setId($id)
    {
        $this->id = (string)$id;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function toXML()
    {
        $xml = "<task>\n";
        
        if ($this->getId() != null)
            $xml .= '<id type="integer">' . $this->getId() . "</id>\n";
        
        if ($this->getRecordingId() != null)
        {
            $xml .= '<recording-id>' . $this->getSubjectId() . "</subject-id>\n";
        }
        
        if ($this->getSubjectId() != null)
        {
            $xml .= '<subject-id>' . $this->getSubjectId() . "</subject-id>\n";
            $xml .= '<subject-type>' . $this->getSubjectType() . "</subject-type>\n";
        }
        
        $xml .= '<body>' . $this->getBody() . "</body>\n";
        $xml .= '<frame>' . $this->getFrame() . "</frame>\n";
        
        if ($this->getCategoryId() != null)
            $xml .= '<category-id>' . $this->getCategoryId() . "</category-id>\n";
        
        if ($this->getOwnerId() != null)	
            $xml .= '<owner-id>' . $this->getOwnerId() . "</owner-id>\n";
        
        if ($this->getDueAt() != null)
            $xml .= '<due-at>' . $this->getDueAt() . "</due-at>\n";
        
        if ($this->getAlertAt() != null)
            $xml .= '<alert-at>' . $this->getAlertAt() . "</alert-at>\n";
        
        if ($this->getPublic() != null)
            $xml .= '<public type="boolean">' . ($this->getPublic() ? "true" : "false") . "</public>\n";
        
        if ($this->getNotify() != null)
            $xml .= '<notify type="boolean">' . ($this->getNotify() ? "true" : "false") . "</notify>\n";
        
        $xml .= "</task>\n";
        
        return $xml;
    }	
    
    public function loadFromXMLObject($xml_obj)
    {
        if ($this->debug)
            print_r($xml_obj);
        
        $this->setId($xml_obj->{'id'});
        $this->setAuthorId($xml_obj->{'author-id'});
        $this->setSubjectId($xml_obj->{'subject-id'});
        $this->setSubjectType($xml_obj->{'subject-type'});
        $this->setSubjectName($xml_obj->{'subject-name'});
        $this->setCategoryId($xml_obj->{'category-id'});
        $this->setBody($xml_obj->{'body'});
        $this->setFrame($xml_obj->{'frame'});
        $this->setDueAt($xml_obj->{'due-at'});
        $this->setAlertAt($xml_obj->{'alert-at'});
        
        $this->setCreatedAt($xml_obj->{'created-at'});
        $this->setUpdatedAt($xml_obj->{'updated-at'});
        $this->setPublic(($xml_obj->{'public'} == "true"));
        
        return true;
    }
    
    public function __construct(Api_Highrise $highrise)
    {
        $this->account = $highrise->account;
        $this->token = $highrise->token;
        $this->debug = $highrise->debug;
        $this->curl = curl_init();	
    }
}