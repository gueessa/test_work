<?php

class Event {

    /**
     * All of the registered events.
     *
     * @var array
     */
    public static $events = array();
    
    /**
     * The queued events waiting for flushing.
     *
     * @var array
     */
    public static $queued = array();
    
    /**
     * All of the registered queue flusher callbacks.
     *
     * @var array
     */
    public static $flushers = array();
    
    /**
     * Determine if an event has any registered listeners.
     *
     * @param string $event
     * @return bool
    */
     public static function listeners($event)
    {
        return isset(static::$events[$event]);
    }
    
    /**
     * Register a callback for a given event.
     *
     * @param string $event
     * @param mixed $callback
     * @return void
     */
    public static function listen($event, $callback)
    {
        static::$events[$event][] = $callback;
    }
    
    /**
     * Override all callbacks for a given event with a new callback.
     *
     * @param string $event
     * @param mixed $callback
     * @return void
     */
    public static function override($event, $callback)
    {
        static::clear($event);
    
        static::listen($event, $callback);
    }
    
    /**
     * Add an item to an event queue for processing.
     *
     * @param string $queue
     * @param string $key
     * @param mixed $data
     * @return void
     */
    public static function queue($queue, $key, $data = array())
    {
        static::$queued[$queue][$key] = $data;
    }
    
    /**
     * Register a queue flusher callback.
     *
     * @param string $queue
     * @param mixed $callback
     * @return void
     */
    public static function flusher($queue, $callback)
    {
        static::$flushers[$queue][] = $callback;
    }
    
    /**
     * Clear all event listeners for a given event.
     *
     * @param string $event
     * @return void
     */
    public static function clear($event)
    {
        unset(static::$events[$event]);
    }
    
    /**
     * Fire an event and return the first response.
     *
     *
     * @param string $event
     * @param array $parameters
     * @return mixed
     */
    public static function first($event, $parameters = array())
    {
        return head(static::fire($event, $parameters));
    }
    
    /**
     * Fire an event and return the first response.
     *
     * Execution will be halted after the first valid response is found.
     *
     * @param string $event
     * @param array $parameters
     * @return mixed
     */
    public static function until($event, $parameters = array())
    {
        return static::fire($event, $parameters, true);
    }
    
    /**
     * Flush an event queue, firing the flusher for each payload.
     *
     * @param string $queue
     * @return void
     */
    public static function flush($queue)
    {
        foreach (static::$flushers[$queue] as $flusher)
        {
            if ( ! isset(static::$queued[$queue])) continue;
    
            foreach (static::$queued[$queue] as $key => $payload)
            {
                array_unshift($payload, $key);
    
                call_user_func_array($flusher, $payload);
            }
        }
    }
    
    /**
     * Fire an event so that all listeners are called.
     *
     *
     * @param string|array $events
     * @param array $parameters
     * @param bool $halt
     * @return array
     */
    public static function fire($events, $parameters = array(), $halt = false)
    {
        $responses = array();
    
        $parameters = (array) $parameters;
    
        foreach ((array) $events as $event)
        {
            if (static::listeners($event))
            {
                foreach (static::$events[$event] as $callback)
                {
                    $response = call_user_func_array($callback, $parameters);
        
                    if ($halt and ! is_null($response))
                    {
                        return $response;
                    }
        
                    $responses[] = $response;
                }
            }
        }
        
        return $halt ? null : $responses;
    }
}