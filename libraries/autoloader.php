<?php

spl_autoload_register(array('Autoloader', 'load'));

class Autoloader {

	/**
 	 * Auto load class  
 	 *
 	 * @return 	bool
 	 */		
	public static function load($class)
  	{
    	if (class_exists($class))
    	{
      		return true;
    	}

        $class = str_replace('_', DS, strtolower($class));
        
    	if (file_exists($file = __DIR__.DS.$class.EXT))
    	{
      		include_once $file;
      		return true;
    	}
    	
    	return false;
  	}
}