<?php

class Config {

    /**
    * All of the loaded configuration items.
    *
    * The configuration arrays are keyed by their owning bundle and file.
    *
    * @var array
    */
    public static $items = array();
    
    /**
    * A cache of the parsed configuration items.
    *
    * @var array
    */
    public static $cache = array();
    
    /**
     * Determine if a configuration item or file exists.
     * 
     * @param string $key
     * @return bool
     */
    public static function has($key)
    {
        return ! is_null(static::get($key));
    }
    
    /**
     * Get a configuration item.
     *
     * @param string $key
     * @param mixed $default
     * @return array
     */
    public static function get($key, $default = null)
    {
        list($file, $item) = static::parse($key);
    
        if ( ! static::load($file)) return value($default);
    
        $items = static::$items[$file];
    
        if (is_null($item))
        {
            return $items;
        }
        else
        {
            return array_get($items, $item, $default);
        }
    }
    
    /**
     * Parse a key and return its file and key segments.
     *
     * Configuration items are named using the {file}.{item} convention.
     *
     * @param string $key
     * @return array
     */
    protected static function parse($key)
    {
        if (array_key_exists($key, static::$cache))
        {
            return static::$cache[$key];
        }
    
        $segments = explode('.', $key);
    
        if (count($segments) >= 2)
        {
            $parsed = array($segments[0], implode('.', array_slice($segments, 1)));
        }
        else
        {
            $parsed = array($segments[0], null);
        }
    
        return static::$cache[$key] = $parsed;
    }
    
    /**
     * Load all of the configuration items from a configuration file.
     *
     * @param string $file
     * @return bool
     */
    public static function load($file)
    {
        if (isset(static::$items[$file])) return true;
    
        $config = static::file($file);
    
        if (count($config) > 0)
        {
            static::$items[$file] = $config;
        }
    
        return isset(static::$items[$file]);
    }
    
    /**
     * Load the configuration items from a configuration file.
     *
     * @param string $file
     * @return array
     */
    public static function file($file)
    {
        $config = array();
        
        $directory = static::path();
        
        if (file_exists($path = $directory.$file.EXT))
        {
            $config = array_merge($config, require $path);        
        }

        return $config;
    }
    
    /**
     * Get the configuration path 
     *
     * @return string
     */
    protected static function path()
    {
        return BASEPATH.'config/';
    }
}