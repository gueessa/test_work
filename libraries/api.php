<?php

class Api {

    /**
     * The currently active api drivers.
     *
     * @var array
     */
    public static $drivers = array();
    
    /**
     * The third-party driver registrar.
     *
     * @var array
     */
    public static $registrar = array();
    
    /**
     * Get an authentication driver instance.
     *
     * @param   string $driver
     * @return  Driver
     */
    public static function make($driver)
    {
        if ( ! isset(static::$drivers[$driver]))
        {
            static::$drivers[$driver] = static::factory($driver);
        }
    
        return static::$drivers[$driver];
    }
    
    /**
     * Create a new authentication driver instance.
     *
     * @param   string $driver
     * @return  Driver
     */
    protected static function factory($driver)
    {
        if (isset(static::$registrar[$driver]))
        {
            $resolver = static::$registrar[$driver];
    
            return $resolver();
        }
    
        switch ($driver)
        {
            case 'highrise':
                return new Api_Highrise();
    
            case 'helpscout':
                return new Api_Helpscout();
    
            default:
                throw new \Exception("Api driver {$driver} is not supported.");
        }
    }
    
    /**
     * Register a third-party api driver.
     *
     * @param   string $driver
     * @param   Closure $resolver
     * @return  void
     */
    public static function extend($driver, Closure $resolver)
    {
        static::$registrar[$driver] = $resolver;
    }
}